package ru.shilov.tm.error;

public final class DateTimeParseException extends RuntimeException {

    public DateTimeParseException() {
        super("Некорректная дата");
    }

}
