package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.ProjectDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.List;

public final class ProjectFindByNameOrDescriptionCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ФРАЗУ ДЛЯ ПОИСКА:");
        @NotNull final String searchPhrase = getServiceLocator().getTerminalService().nextLine();
        @NotNull final List<ProjectDTO> projects = getEndPointLocator().getProjectEndPoint().findProjectsByNameOrDescription(token, searchPhrase);
        getServiceLocator().getTerminalService().printAllProjects(projects);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск проектов";
    }

}
