package ru.shilov.tm.command.user;

import com.google.common.base.Strings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.Role;
import ru.shilov.tm.api.endpoint.UserDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.Arrays;
import java.util.List;

public final class UserMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        @NotNull final String userNumber = getServiceLocator().getTerminalService().nextLine();
        if (!numberCheck(userNumber)) throw new Exception("Неверный ID пользователя");
        @NotNull final List<UserDTO> users = getEndPointLocator().getUserEndPoint().findAllUsers(token);
        @NotNull final UserDTO u = users.get(Integer.parseInt(userNumber) - 1);
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        u.setLogin(getServiceLocator().getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(getServiceLocator().getTerminalService().nextLine());
        Arrays.asList(Role.values()).forEach(r -> System.out.println(String.format("%d. %s", r.ordinal() + 1, r.value())));
        System.out.println("ВЫБЕРИТЕ РОЛЬ:");
        @NotNull String roleId = getServiceLocator().getTerminalService().nextLine();
        while (!roleCheck(roleId)) {
            System.out.println("ВЫБЕРИТЕ РОЛЬ:");
            roleId = getServiceLocator().getTerminalService().nextLine();
        }
        u.setRole(Role.values()[Integer.parseInt(roleId) - 1]);
        getEndPointLocator().getUserEndPoint().mergeUser(token, u);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Редактирование пользователя";
    }

    @NotNull
    private Boolean roleCheck(@Nullable final String roleId) {
        return !Strings.isNullOrEmpty(roleId)
                && roleId.matches("\\d+")
                && Integer.parseInt(roleId) <= Role.values().length;
    }

}
