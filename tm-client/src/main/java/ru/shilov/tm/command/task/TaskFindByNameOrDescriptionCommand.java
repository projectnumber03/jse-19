package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.TaskDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.List;

public class TaskFindByNameOrDescriptionCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ФРАЗУ ДЛЯ ПОИСКА:");
        @NotNull final String searchPhrase = getServiceLocator().getTerminalService().nextLine();
        @NotNull final List<TaskDTO> tasks = getEndPointLocator().getTaskEndPoint().findTasksByNameOrDescription(getToken(), searchPhrase);
        getServiceLocator().getTerminalService().printAllTasks(tasks);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск задач";
    }

}
