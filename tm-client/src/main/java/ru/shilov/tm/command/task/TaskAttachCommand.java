package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.ProjectDTO;
import ru.shilov.tm.api.endpoint.TaskDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.List;

public final class TaskAttachCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        @NotNull final String taskNumber = getServiceLocator().getTerminalService().nextLine();
        if (!numberCheck(taskNumber)) throw new Exception("Неверный ID задачи");
        @NotNull final List<TaskDTO> tasks = getEndPointLocator().getTaskEndPoint().findTasksByUserId(token);
        @NotNull final TaskDTO t = tasks.get(Integer.parseInt(taskNumber) - 1);
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectNumber = getServiceLocator().getTerminalService().nextLine();
        if (!numberCheck(projectNumber)) throw new Exception("Неверный ID проекта");
        @NotNull final List<ProjectDTO> projects = getEndPointLocator().getProjectEndPoint().findProjectsByUserId(token);
        @NotNull final ProjectDTO p = projects.get(Integer.parseInt(projectNumber) - 1);
        t.setProjectId(p.getId());
        getEndPointLocator().getTaskEndPoint().mergeTask(token, t);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-attach";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Добавление задачи в проект";
    }

}
