import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.shilov.tm.api.endpoint.Role;
import ru.shilov.tm.api.endpoint.UserDTO;
import ru.shilov.tm.context.Bootstrap;

import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UserOperationTest {

    class UserFactory {

        private int count = 1;

        private UserDTO createUser() {
            @NotNull final UserDTO u = new UserDTO();
            u.setLogin("user" + count);
            u.setPassword("pass" + count);
            u.setRole(Role.USER);
            count++;
            return u;
        }

        private void createUsers(@NotNull final Bootstrap b, final int count) throws Exception {
            for (int i = 0; i < count; i++) {
                b.getUserEndPoint().persistUser(b.getToken(), createUser());
            }
        }

    }

    @Before
    public void registerUser() throws Exception {
        @NotNull final Bootstrap b = new Bootstrap();
        @NotNull final UserDTO testUser = new UserDTO();
        testUser.setLogin("test");
        testUser.setPassword("test");
        testUser.setRole(Role.ADMIN);
        b.getUserEndPoint().registerUser(testUser);
    }

    @After
    public void deleteUser() throws Exception {
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        @Nullable final String idToDelete = b.getUserEndPoint().findAllUsers(b.getToken()).stream()
                .filter(u -> "test".equals(u.getLogin()))
                .map(UserDTO::getId).findAny().orElse(null);
        b.getUserEndPoint().removeOneUserById(b.getToken(), idToDelete);
    }

    @Test
    public void findOneTest() throws Exception {
        @NotNull final UserFactory uf = new UserFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        uf.createUsers(b, 10);
        @Nullable final List<UserDTO> allUsers = b.getUserEndPoint().findAllUsers(b.getToken());
        assertNotNull(allUsers);
        @Nullable final UserDTO wantedUser = allUsers.stream().filter(u -> "user2".equals(u.getLogin())).findAny().orElse(null);
        assertNotNull(wantedUser);
        @Nullable final UserDTO foundedUser = b.getUserEndPoint().findOneUserById(b.getToken(), wantedUser.getId());
        assertEquals(wantedUser.getLogin(), foundedUser.getLogin());
        assertEquals(wantedUser.getRole(), foundedUser.getRole());
        for (UserDTO u : allUsers.stream().filter(u -> u.getLogin().matches("user\\d+")).collect(Collectors.toList())) {
            b.getUserEndPoint().removeOneUserById(b.getToken(), u.getId());
        }
    }

    @Test
    public void mergeTest() throws Exception {
        @NotNull final UserFactory uf = new UserFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        uf.createUsers(b, 1);
        @NotNull final List<UserDTO> usersBeforeMerge = b.getUserEndPoint().findAllUsers(b.getToken());
        @Nullable final UserDTO userToMerge = usersBeforeMerge.stream().filter(u -> "user1".equals(u.getLogin())).findAny().orElse(null);
        assertNotNull(userToMerge);
        userToMerge.setLogin("user2");
        userToMerge.setRole(Role.ADMIN);
        b.getUserEndPoint().mergeUser(b.getToken(), userToMerge);
        @NotNull final List<UserDTO> usersAfterMerge = b.getUserEndPoint().findAllUsers(b.getToken());
        assertEquals(usersBeforeMerge.size(), usersAfterMerge.size());
        @Nullable final UserDTO empty = b.getUserEndPoint().findAllUsers(b.getToken()).stream().filter(u -> "user1".equals(u.getLogin())).findAny().orElse(null);
        assertNull(empty);
        @Nullable final UserDTO user = b.getUserEndPoint().findAllUsers(b.getToken()).stream().filter(u -> "user2".equals(u.getLogin())).findAny().orElse(null);
        assertNotNull(user);
        assertEquals(user.getLogin(), "user2");
        assertEquals(user.getRole(), Role.ADMIN);
        b.getUserEndPoint().removeOneUserById(b.getToken(), user.getId());
    }

    @Test
    public void persistTest() throws Exception {
        @NotNull final UserFactory uf = new UserFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        long countBeforePersist = b.getUserEndPoint().findAllUsers(b.getToken()).size();
        b.getUserEndPoint().persistUser(b.getToken(), uf.createUser());
        @NotNull final List<UserDTO> usersAfterPersist = b.getUserEndPoint().findAllUsers(b.getToken());
        @Nullable final UserDTO user = usersAfterPersist.stream().filter(u -> "user1".equals(u.getLogin())).findAny().orElse(null);
        assertNotNull(user);
        assertEquals(user.getLogin(), "user1");
        assertEquals(user.getRole(), Role.USER);
        assertEquals(++countBeforePersist, usersAfterPersist.size());
        b.getUserEndPoint().removeOneUserById(b.getToken(), user.getId());
    }

    @Test
    public void removeTest() throws Exception {
        @NotNull final UserFactory uf = new UserFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getUserEndPoint().persistUser(b.getToken(), uf.createUser());
        @NotNull final List<UserDTO> usersBeforeRemove = b.getUserEndPoint().findAllUsers(b.getToken());
        @Nullable final UserDTO userToRemove = usersBeforeRemove.stream().filter(u -> "user1".equals(u.getLogin())).findAny().orElse(null);
        b.getUserEndPoint().removeOneUserById(b.getToken(), userToRemove.getId());
        @NotNull final List<UserDTO> usersAfterRemove = b.getUserEndPoint().findAllUsers(b.getToken());
        assertEquals(usersBeforeRemove.size(), usersAfterRemove.size() + 1);
    }

}
