package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.repository.TaskRepositoryImpl;

import javax.persistence.EntityManager;
import java.util.List;

@SuperBuilder
public final class TaskServiceImpl extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Override
    public List<Task> findByUserId(@Nullable final String userId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId)) throw new NoSuchEntityException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        return TaskRepositoryImpl.builder().entityManager(em).build().findByUserId(userId);
    }

    @Override
    public void removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        TaskRepositoryImpl.builder().entityManager(em).build().removeByUserId(userId);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public List<Task> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(projectId)) throw new NoSuchEntityException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        return TaskRepositoryImpl.builder().entityManager(em).build().findByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDescription(@Nullable String userId, @Nullable String value) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(value)) throw new NoSuchEntityException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        return TaskRepositoryImpl.builder().entityManager(em).build().findByNameOrDescription(userId, value);
    }

    @Override
    public void removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        TaskRepositoryImpl.builder().entityManager(em).build().removeOneByUserId(userId, id);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public List<Task> findByUserIdOrderBy(@NotNull final String userId, @NotNull final String field) {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(field)) throw new NoSuchEntityException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        return TaskRepositoryImpl.builder().entityManager(em).build().findByUserIdOrderBy(userId, field);
    }

    @Override
    public IRepository<Task> getRepository(@NotNull final EntityManager em) {
        return TaskRepositoryImpl.builder().entityManager(em).build();
    }

}
