package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityMergeException;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.repository.UserRepositoryImpl;
import ru.shilov.tm.util.SignatureUtil;

import javax.persistence.EntityManager;

@SuperBuilder
public final class UserServiceImpl extends AbstractService<User> implements IUserService {

    @Override
    public void removeOneById(@Nullable final String id) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(id)) throw new EntityRemoveException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        UserRepositoryImpl.builder().entityManager(em).build().removeOneById(id);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public User register(@Nullable final User user) {
        if (user == null) throw new EntityPersistException();
        @NotNull final String salt = bootstrap.getSettingService().getProperty("salt");
        @NotNull final String cycle = bootstrap.getSettingService().getProperty("cycle");
        user.setPassword(SignatureUtil.sign(user.getPassword(), salt, Integer.parseInt(cycle)));
        persist(user);
        return user;
    }

    @Nullable
    @Override
    public User merge(@Nullable final User user) throws RuntimeException {
        if (user == null) throw new EntityMergeException();
        @NotNull final String salt = bootstrap.getSettingService().getProperty("salt");
        @NotNull final String cycle = bootstrap.getSettingService().getProperty("cycle");
        user.setPassword(SignatureUtil.sign(user.getPassword(), salt, Integer.parseInt(cycle)));
        return super.merge(user);
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws NoSuchEntityException {
        @Nullable final User user;
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        if (Strings.isNullOrEmpty(login) || ((user = UserRepositoryImpl.builder().entityManager(em).build().findByLogin(login)) == null)){
            throw new NoSuchEntityException();
        }
        return user;
    }

    @Override
    public IRepository<User> getRepository(@NotNull final EntityManager em) {
        return UserRepositoryImpl.builder().entityManager(em).build();
    }

}
