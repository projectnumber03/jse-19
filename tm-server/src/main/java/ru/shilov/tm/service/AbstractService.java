package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.service.IService;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.entity.AbstractEntity;
import ru.shilov.tm.error.EntityMergeException;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.NoSuchEntityException;

import javax.persistence.EntityManager;
import java.util.List;

@SuperBuilder
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    protected Bootstrap bootstrap;

    @NotNull
    @Override
    public List<T> findAll() {
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        @NotNull final List<T> entities = getRepository(em).findAll();
        em.close();
        return entities;
    }

    @NotNull
    @Override
    public T findOne(@Nullable final String id) throws RuntimeException {
        @Nullable final T entity;
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        if (Strings.isNullOrEmpty(id) || (entity = getRepository(em).findOne(id)) == null) throw new NoSuchEntityException();
        return entity;
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        getRepository(em).removeAll();
        em.getTransaction().commit();
        em.close();
    }

    @Nullable
    @Override
    public T persist(@Nullable final T entity) {
        if (entity == null) throw new EntityPersistException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        getRepository(em).persist(entity);
        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Nullable
    @Override
    public T merge(@Nullable final T entity) {
        if (entity == null) throw new EntityMergeException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        getRepository(em).merge(entity);
        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public abstract IRepository<T> getRepository(@NotNull final EntityManager em);

}
