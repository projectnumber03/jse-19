package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.ISettingRepository;
import ru.shilov.tm.api.service.ISettingService;
import ru.shilov.tm.error.NoSuchEntityException;

@AllArgsConstructor
public final class SettingServiceImpl implements ISettingService {

    @Getter
    @NotNull
    private final ISettingRepository repository;

    @NotNull
    @Override
    public String getProperty(@Nullable final String name) {
        @Nullable final String property;
        if (Strings.isNullOrEmpty(name) || (property = repository.getProperty(name)) == null) throw new NoSuchEntityException();
        return property;
    }

}
