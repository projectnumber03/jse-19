package ru.shilov.tm.error;

public final class EntityMergeException extends RuntimeException {

    public EntityMergeException() {
        super("Ошибка обновления объекта");
    }

    public EntityMergeException(Throwable throwable) {
        super("Ошибка добавления объекта", throwable);
    }

}
