package ru.shilov.tm.error;

public final class DecryptException extends RuntimeException {

    public DecryptException() {
        super("Ошибка дешифровки");
    }

}
