package ru.shilov.tm.error;

import org.jetbrains.annotations.NotNull;

public final class NumberToIdTransformException extends RuntimeException {

    public NumberToIdTransformException(@NotNull final String value) {
        super(String.format("Ошибка получения id %s", value));
    }

    public NumberToIdTransformException(@NotNull final String s, @NotNull final Throwable throwable) {
        super(s, throwable);
    }

    public NumberToIdTransformException() {
        super("Ошибка получения id");
    }

}
