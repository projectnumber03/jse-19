package ru.shilov.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.service.ISettingService;
import ru.shilov.tm.entity.AbstractEntity;
import ru.shilov.tm.repository.SettingRepositoryImpl;
import ru.shilov.tm.service.SettingServiceImpl;

import javax.jms.*;
import javax.persistence.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EntityListener {

    @NotNull
    private final ISettingService messageService = new SettingServiceImpl(new SettingRepositoryImpl("message.properties"));

    @NotNull
    private final ISettingService settingService = new SettingServiceImpl(new SettingRepositoryImpl("application.properties"));

    @PostLoad
    public void postLoad(@NotNull final AbstractEntity entity) {
        @NotNull final String encodedMsg = new String(messageService.getProperty("post.load.message").getBytes(StandardCharsets.ISO_8859_1));
        send(String.format(encodedMsg, now(), entityToJson(entity)), entity.getClass().getSimpleName());
    }

    @PrePersist
    public void prePersist(@NotNull final AbstractEntity entity) {
        @NotNull final String encodedMsg = new String(messageService.getProperty("pre.persist.message").getBytes(StandardCharsets.ISO_8859_1));
        send(String.format(encodedMsg, now(), entityToJson(entity)), entity.getClass().getSimpleName());
    }

    @PostPersist
    public void postPersist(@NotNull final AbstractEntity entity) {
        @NotNull final String encodedMsg = new String(messageService.getProperty("post.persist.message").getBytes(StandardCharsets.ISO_8859_1));
        send(String.format(encodedMsg, now(), entityToJson(entity)), entity.getClass().getSimpleName());
    }

    @PreRemove
    public void preRemove(@NotNull final AbstractEntity entity) {
        @NotNull final String formattedMsg = new String(messageService.getProperty("pre.remove.message").getBytes(StandardCharsets.ISO_8859_1));
        @NotNull final String message = String.format(formattedMsg, now(), entityToJson(entity));
        send(message, entity.getClass().getSimpleName());
    }

    @PostRemove
    public void postRemove(@NotNull final AbstractEntity entity) {
        @NotNull final String encodedMsg = new String(messageService.getProperty("post.remove.message").getBytes(StandardCharsets.ISO_8859_1));
        send(String.format(encodedMsg, now(), entityToJson(entity)), entity.getClass().getSimpleName());
    }

    @PreUpdate
    public void preUpdate(@NotNull final AbstractEntity entity) {
        @NotNull final String encodedMsg = new String(messageService.getProperty("pre.update.message").getBytes(StandardCharsets.ISO_8859_1));
        send(String.format(encodedMsg, now(), entityToJson(entity)), entity.getClass().getSimpleName());
    }

    @PostUpdate
    public void postUpdate(@NotNull final AbstractEntity entity) {
        @NotNull final String encodedMsg = new String(messageService.getProperty("post.update.message").getBytes(StandardCharsets.ISO_8859_1));
        send(String.format(encodedMsg, now(), entityToJson(entity)), entity.getClass().getSimpleName());
    }

    @SneakyThrows
    private String entityToJson(@NotNull final AbstractEntity entity) {
        return new ObjectMapper().writeValueAsString(entity.toDTO());
    }

    @SneakyThrows
    private void send(@NotNull final String message, @NotNull final String topicName) {
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(settingService.getProperty("jms.url"));
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(topicName);
        @NotNull final MessageProducer producer = session.createProducer(destination);
        @NotNull final TextMessage textMessage = session.createTextMessage(message);
        producer.send(textMessage);
        connection.close();
    }

    private String now() {
        return DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss").format(LocalDateTime.now());
    }

}
