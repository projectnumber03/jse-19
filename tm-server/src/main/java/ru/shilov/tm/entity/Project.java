package ru.shilov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.dto.ProjectDTO;
import ru.shilov.tm.enumerated.Status;
import ru.shilov.tm.listener.EntityListener;
import ru.shilov.tm.util.LocalDateAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "app_project")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project extends AbstractEntity {

    @NotNull
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate start;

    @Nullable
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate finish;

    @Nullable
    @ManyToOne
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @Override
    public ProjectDTO toDTO() {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(getId());
        if (getUser() != null) projectDTO.setUserId(getUser().getId());
        projectDTO.setName(getName());
        projectDTO.setDescription(getDescription());
        if (getStart() != null)
            projectDTO.setStart(DateTimeFormatter.ISO_LOCAL_DATE.format(getStart()));
        if (getFinish() != null)
            projectDTO.setFinish(DateTimeFormatter.ISO_LOCAL_DATE.format(getFinish()));
        projectDTO.setStatus(getStatus());
        return projectDTO;
    }

}
