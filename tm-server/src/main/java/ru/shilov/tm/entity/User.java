package ru.shilov.tm.entity;

import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.dto.UserDTO;
import ru.shilov.tm.listener.EntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@RequiredArgsConstructor
@Entity(name = "app_user")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class User extends AbstractEntity {

    @NotNull
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Session> sessions = new ArrayList<>();

    @NonNull
    @Nullable
    @Column(unique = true)
    private String login;

    @NonNull
    @Nullable
    private String password;

    @NonNull
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role;

    @NotNull
    @Override
    public UserDTO toDTO() {
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(getId());
        userDTO.setLogin(getLogin());
        userDTO.setPassword(getPassword());
        userDTO.setRole(getRole());
        return userDTO;
    }

    @Getter
    @AllArgsConstructor
    public enum Role {

        USER("Пользователь"),
        ADMIN("Администратор");

        private final String description;

    }

}
