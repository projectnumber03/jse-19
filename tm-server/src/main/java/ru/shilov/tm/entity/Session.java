package ru.shilov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.dto.SessionDTO;
import ru.shilov.tm.listener.EntityListener;
import ru.shilov.tm.util.LocalDateTimeAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "app_session")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractEntity {

    @Nullable
    @ManyToOne
    @JsonIgnore
    private User user;

    @Nullable
    @Enumerated(EnumType.STRING)
    private User.Role role;

    @NotNull
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime creationDate = LocalDateTime.now();

    @Nullable
    private String signature;

    @NotNull
    @Override
    public SessionDTO toDTO() {
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(getId());
        if (getUser() != null) sessionDTO.setUserId(getUser().getId());
        sessionDTO.setRole(getRole());
        return sessionDTO;
    }

}

