package ru.shilov.tm.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.dto.TaskDTO;
import ru.shilov.tm.enumerated.Status;
import ru.shilov.tm.listener.EntityListener;
import ru.shilov.tm.util.LocalDateAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "app_task")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate start;

    @Nullable
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate finish;

    @Nullable
    @ManyToOne
    private Project project;

    @Nullable
    @ManyToOne
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @Override
    public TaskDTO toDTO() {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(getId());
        taskDTO.setUserId(getUser().getId());
        if(getProject() != null) {
            taskDTO.setProjectId(getProject().getId());
        }
        taskDTO.setName(getName());
        taskDTO.setDescription(getDescription());
        if (getStart() != null) taskDTO.setStart(DateTimeFormatter.ISO_LOCAL_DATE.format(getStart()));
        if (getFinish() != null) taskDTO.setFinish(DateTimeFormatter.ISO_LOCAL_DATE.format(getFinish()));
        taskDTO.setStatus(getStatus());
        return taskDTO;
    }

}
