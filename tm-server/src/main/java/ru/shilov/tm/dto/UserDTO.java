package ru.shilov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.entity.User;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class UserDTO extends AbstractEntityDTO implements Serializable {

    private static final long SerialVersionUID = 1L;

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private User.Role role;

    @NotNull
    public static User toUser(@NotNull final Bootstrap bootstrap, @NotNull final UserDTO userDTO) {
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        user.setRole(userDTO.getRole());
        user.setProjects(bootstrap.getProjectService().findByUserId(userDTO.getId()));
        user.setTasks(bootstrap.getTaskService().findByUserId(userDTO.getId()));
        return user;
    }
}
