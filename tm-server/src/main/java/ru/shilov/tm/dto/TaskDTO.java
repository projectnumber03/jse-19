package ru.shilov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.enumerated.Status;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@NoArgsConstructor
public class TaskDTO extends AbstractEntityDTO implements Serializable {

    private static final long SerialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String start;

    @NotNull
    private String finish;

    @Nullable
    private String projectId;

    @Nullable
    private String userId;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    public static Task toTask(@NotNull final Bootstrap bootstrap, @NotNull final TaskDTO taskDTO) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setUser(bootstrap.getUserService().findOne(taskDTO.getUserId()));
        if (taskDTO.getProjectId() != null) task.setProject(bootstrap.getProjectService().findOne(taskDTO.getProjectId()));
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setStart(LocalDate.parse(taskDTO.getStart(), DateTimeFormatter.ISO_LOCAL_DATE));
        task.setFinish(LocalDate.parse(taskDTO.getFinish(), DateTimeFormatter.ISO_LOCAL_DATE));
        task.setStatus(taskDTO.getStatus());
        return task;
    }

}
