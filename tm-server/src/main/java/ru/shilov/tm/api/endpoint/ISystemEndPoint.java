package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISystemEndPoint {

    @NotNull
    @WebMethod
    String getHost(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    String getPort(@Nullable final String token) throws Exception;

}
