package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;

import java.util.List;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session findOneByUserId(@Nullable final String userId, @Nullable final String id) throws NoSuchEntityException;

    void removeByUserId(@Nullable final String userId) throws EntityRemoveException;

    @Nullable
    Session create(@NotNull final String login, @NotNull final String password) throws Exception;

    Session validate(@Nullable final String userSession) throws Exception;

    Session validate(@Nullable final String userSession, @NotNull final List<User.Role> roles) throws Exception;

    Session decrypt(@Nullable final String userSession) throws Exception;

}
